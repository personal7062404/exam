const express = require("express");
const app = express();
const mysql = require("mysql2");
const cors = require("cors");


const connection = mysql.createConnection({
  host: "db",
  user: "root",
  password: "root",
  waitForConnections: true,
  connectionLimit: 20,
  database: "test",
});

app.use(cors("*"));
app.use(express.json());

app.get("/student" , (req,res) => {
    connection.query("select * from student",
        (err , results) =>{
            res.send(results);
        }
    )
    
})

app.post("/student", (req, res) => {
  var query = `insert into student values("${req.body.SName}" , "${req.body.Password}" , "${req.body.Course}"  , "${req.body.PassingYear}", ${req.body.PRNO}, "${req.body.DOB}")`;

  connection.query(query, (error, result) => {
    if (error != null) {
      res.send(error);
    } else {
      res.send(result);
    }
  });
});

app.put("/student/:no", (req, res) => {
  var query = `update student set course = '${req.body.Course}', 
                                                prn_no = '${req.body.PRNO}' 
                                                where student_id = ${req.params.no}`;
  connection.query(query, (error, result) => {
    if (error != null) {
      res.send(error);
    } else {
      res.send(result);
    }
  });
});

app.delete("/student/:dob", (req, res) => {
  var query = `delete from student where dob = '${req.params.dob}'`;

  connection.query(query, (error, result) => {
    if (error != null) {
      res.send(error);
    } else {
      res.send(result);
    }
  });
});


app.listen(4000 ,"0.0.0.0" , () =>{
    console.log("Server is up and running");
})